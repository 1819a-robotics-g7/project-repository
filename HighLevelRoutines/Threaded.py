
# We are working in polar coorinates (angle-distance)
# tune robot detection paramaters for trahs-balls
# tune robot detection paramaters for trahs-bin
# put at start position and scan for trash
# scan for bin
# recursion or cycle
#  find nearest trash
#  move to trash
#  catch it
#  rescan horizon, fill trashcoords again
# stop if no trash or minimal limit fulfillled (6 balls)
# rescan for bin
# move to bin
# empty robot
import cv2
import serial
from threading import Thread, Lock


class Robot(object):
    def __init__(self):
        self.cam = Camera(self)
        self.cam_info = None
        self.comms = Comms(self)
        self.comms_info = None

        self.cams.start()
        self.comms.start()

    def start(self):
        while True:
            # highest-level logic goes here, use cam_info as input
            # as a result, set comms_info that gets constantly read by Comms
            # thread and outputs motion
            # self.comms_info = ...
            pass


class Camera(Thread):
    def __init__(self, parent):
        Thread.__init__(self)
        self.robot = parent
        self.currframe = None
        self.cap = cv2.VideoCapture(0)

    def run(self):

        while True:

            k, self.currframe = self.cap.read()
            # do image processing
            # get as result, for example, dict of relevant objects and pos.:
            # cam_info={"balls": ((x, y), (x2, y2), (x3, y3)), 'bin': (x4, y4)}
            # self.robot.cam_info = cam_info
            # atomic attribute change op, therefore thread-safe
            pass


class Comms(Thread):
    def __init__(self, parent):
        Thread.__init__(self)
        self.robot = parent

        # this probably has to be worked on to avoid errors
        for i in range(4):
            try:
                self.s = serial.Serial("/dev/ttyUSB" + str(i), baudrate=19200)
            except serial.SerialException:
                print("serial {} not found".format(i))
                continue

        if not self.s.isOpen():
            pass  # handle it

        self.s.write("{{{ INIT MESSAGE FOR ARDUINO }}}")

    def run(self):
        # use self.parent.comms_info to send correct messages to the robot
        pass


if __name__ == "__main__":
    robot = Robot()
    robot.start()

