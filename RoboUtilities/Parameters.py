#place for Trackbar and save-load parameters code
import json
import cv2
#HSV for balls, HSV for trash_place, ROI, speed, P,I,D with default values
params = {"bLH":0, "bLS":0, "bLV":0, "tLH":255,"tLS":255,"tLV":255,"ROI":20,
          "gospeed":50, "P":0.5, "I":0.1,"D":0.3}
#params limits for trackbars. Third value in list mean divider, to get actual parameter
param_limits = {"bLH":[0,255,1], "bLS":[0,255,1], "bLV":[0,255,1],
                "tLH":[0,255,1],"tLS":[0,255,1],"tLV":[0,255,1],"ROI":[1,60,1],
                "gospeed":[0,255,1], "P":[0,10,100], "I":[0,40,100],"D":[0,10,100]}

#save parameters dictionary with json serializer
def save_params(pdict, name):
    if  not fname is None:
        json.dump(pdict, open(fname,'w'))
        
#load prameters into dictionary with json deserializer
def load_params(name, pms):
    pms.update(json.load(open(name)))


def update_params(n):
    global params, tname #unfortunately, Python version of callback doesn't accept user_data
    for key in params:
        params[key] = cv2.getTrackbarPos(key, tname)/param_limits[key][2]

   

# create trackbar panel from parameters dictionary
def create_param_panel(name, pms, pms_lim):
    cv2.namedWindow(name,2)
    for key in pms:
        val = int(pms[key]*pms_lim[key][2])
        if val < pms_lim[key][0]:
            val = pms_lim[key][0]
        if val > pms_lim[key][1]:
            val = pms_lim[key][0]       
        cv2.createTrackbar(key, name, val, pms_lim[key][1], update_params)




fname = "testparams.txt"
tname = "Roboset"

load_params(fname, params)
create_param_panel(tname, params, param_limits)
update_params(0)
while True:
    q = cv2.waitKey(1)
    if q & 0xFF == ord('q'):
        break
save_params(params, fname)
cv2.destroyAllWindows()
