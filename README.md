# Project repository: Trash collecting robot

## Members:
1. Mart Kaasiku
2. Mateus Surrage Reis
3. Sergei Dolgov
4.Taaniel K�la

###Task description

This is a slight modification to the trash cleaning robot assignment. As such we will only describe the differences.

This robot is going to have ROS running on Raspberry Pi. The Raspberry will give commands over serial to an Arduino.
The Arduino will control a motor controller (LN298N). The aim of this robot is to collect all ping-pong balls in a given area
and transport them to a designated area. Ping-pong balls are used instead of golf balls because they are lighter and smaller.
We are going to fit all the balls inside the robot for transport. For this reason we need a custom made chassis. The robot is going to aim
for the ball, and a mechanism inside is going to scoop it up and after reaching the destination, the mechanism is going to run in reverse to
release the balls.

###Component List

| Component     | Quantity          | Do we have it |
| ------------- |:-------------:	| -----:		|
| Arduino     	| 1 				|Yes 			|
| Raspberry Pi3 | 1					|No 			|
| LN298N 		| 1      			|Yes			|
| Encoders		| 2					|Yes			|
| Battery		| 1					|No				|
| Motors		| 2					|No				|
| USB Cable		| 1					|Yes			|
| Camera		| 1					|No				|
| Ping-Pong balls|12				|Yes			|
| Robot platform| 1					|No, need to make|
| Jumpers		| TBC				|Yes			|


###Schedules

####Pair A (Sergei and Mateus)
 by 19.11.2018 Designing Game logic
 	Thinking of how to solve the problem. Upload a schematic
	
 by 26.11.2018 Vision and Serial control
 	Robot will need to recognise orange ping-pong ball, center on it and drive to it.
	For testing classroom robots can be used (there are some orange ping-pong balls in the locker)
	Along with the Raspi controlling the GoPiGo, it'll need to send serial commands to Arduino
	
	POSTER
	
by 3.12.2018 Integrating independent solutions with the other team using ROS

by 10.12.2018 Troubleshooting done. The robot mostly does what it needs to do

####Pair B (Mart and Taaniel)
by 19.11.2018 Designing the new platform. Agreeing on the serial commands that will be used with the other team

by 26.11.2018 Have the new platform ready with hardware attached. 
	Arduino will be able to take serial commands and process them into movements
	
	POSTER

by 3.12.2018 Integrating independent solutions with the other team using ROS

by 10.12.2018 Troubleshooting done. The robot mostly does what it needs to do
--------------------------------------------------------------------------------------------
My notice (Serge)
We can do mechanics/robot part for that task in simpler way:
simpler version is catch-ring, about 1 cm wide and about 6cm in diameter, controlled by servo. When we approached ball, we put this catch down by servo.
and driving to trash place with ball in catch (it rolls inside)
when we reach trash-place, we put the ring up.
I will add picture ASAP.

######  Challenges and Solutions
Mechanics - still unclear best way to collect all balls at once. Initially proposed mechanics looks unreliable. Sumplest one tends to bounce balls
Navigation - it seems we need something like to 303-sensor to know also absolute angle
Computer vision related to navigation - we don't know how is bordered - walls, black tape or just free area with balls. 
