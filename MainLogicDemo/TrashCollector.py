#ESTIMATIONS:
#WE HAVE 4*4 meters area.
#Trash-collector and trash-bin are placed somewhere inside this area
#trash-ball size is about 3-4 cm
import random as r
import turtle as t
import math
import time
import pygame
import easygui as e
import copy
import json

#import easygui
wn = t.Screen()        # creates a graphics window
p = t.Turtle()
p.speed(0)
#area to collect garbage from. Probably matrix 100*100 is enough for 4m*4m,
#cells size is 4cm*4cm

#translation of matrix coords into Turtle coords
xDim = 0 #len(place)
yDim = 0 #len(place[0])
scale = 5
xShift = 0#-xDim*scale/2
yShift = 0#-xDim*scale/2
collected =0#
finish = [0,0]#
trashcoords= []
#visible trash coordinates (which is not obscured by some other trash ball)
#trashcoords = [[0 for x in range(100)] for y in range(2)]
#generating area with trash for algorhytm testing
def fillWithTrash(field, numOfBalls):
    robot = [0,0]
    tbin = [0,0]
    for i in range(numOfBalls):
        x = r.randint(0,99)
        y = r.randint(0,99)
        if i == 0:
            field[x][y] = 2 #trash-bin
            tbin = [x,y]
        elif i==1:
            field[x][y] = 3 #robot
            robot = [x,y] 
        else:
            field[x][y] = 1 #trash 
    return (field, robot, tbin)
def drawBall(p, x,y, rad=4, color=("orange")):
    oldcolor = p.pencolor()
    p.pen(fillcolor=color, pencolor=color)
    p.up()
    p.setpos(x,y)
    p.down()
    p.begin_fill()
    p.circle(rad)
    p.end_fill()
    p.up()
    #
    p.pencolor(oldcolor)
    #pass
#drawing place with garbage
def drawAreaTurtle(p, area):
    global yDim, xDim, xShift, yShift, scale
    p.setpos(0,0)
    xDim = len(area)
    yDim = len(area[0])
    xShift = -xDim*scale/2
    yShift = -xDim*scale/2
    oldspeed = p.speed()
    p.speed(0)
    p.up()
    p.setpos(xShift, yShift)
    #p.setposy(100)
    p.down()
    p.forward(xDim*scale)           # tell alex to move forward by 150 units
    p.left(90)               # turn by 90 degrees
    p.forward(xDim*scale)
    p.left(90)
    p.forward(xDim*scale)
    p.left(90)
    p.forward(yDim*scale)
    p.up()
    p.setpos(0,0)
    for x in range(xDim):
        for y in range(yDim):
            if area[x][y] == 1:
                drawBall(p,(x*scale+xShift),(y*scale+yShift), 4, "orange")
            elif area[x][y] == 2:
                drawBall(p,(x*scale+xShift),(y*scale+yShift), 4, "green")
    p.speed(oldspeed)
    
def findAllTrash(area,trashcoords):
    robopos =[0,0]
    trahspos = [0,0]
    for x in range(xDim-1):
        for y in range(yDim-1):
            if area[x][y] == 1:
                trashcoords.append([x,y])
            elif area[x][y] == 2:
                trahspos[0] = x
                trahspos[1] = y
            elif area[x][y] == 3:
                robopos[0] = x
                robopos[1] = y
                
    del trashcoords[0]
    return trashcoords, robopos, trahspos
        
#We will have here very different, vector-angular solution, but for modelling it needs some awful tricks
#use trashcoords for effective solution !!!!
def findNearestTrash(area, robotpos):
    trashpos = [0,0]
    robx=robotpos[0]
    roby=robotpos[1]
    hdist = 0
    vdist = 0
    mindist = (xDim-1)*(xDim-1) +(yDim-1)*(yDim-1)
    mincoord = list(robotpos)
    for x in range(xDim-1):
        for y in range(yDim-1):
            if area[x][y] == 1 and x!= robx and y != roby:
                hdist = abs(x - robx)
                vdist = abs(y - roby)
                if hdist*hdist +vdist*vdist <= mindist:
                    mincoord[0]=x
                    mincoord[1]=y
                    mindist = hdist*hdist + vdist*vdist
    if hdist == 0 and vdist == 0:
        mindist =0   
    return (mincoord, mindist)

def moveCollector(area, start, to, num):
    global yDim, xDim, xShift, yShift, scale
    oldspeed = p.speed()
    p.speed(5)
    p.setpos(start[0]*scale+xShift,start[1]*scale+yShift)
    p.color("black")
    p.down()
    p.write(str(num))
    p.setpos(to[0]*scale+xShift,to[1]*scale+yShift)
    p.up()
    drawBall(p, to[0]*scale+xShift,to[1]*scale+yShift, rad=4, color=("white"))
    p.down()
    p.write(str(num+1))
    p.up()
    p.speed(oldspeed)
    return num+1
    
    #((0,0),(0,0))
    #hardware people to implement together with software people. PID-navigation to implement
    #softare people ara drawing - clean ld position, draw ne position
    
    
def pullupTrash(pos):
    #Hardware people to implement
    #remove item from matrix for software people
    pass
def removeItem(field, where, to):
    global collected
    #way to trash
    #moveCollector((0,0),(0,0))
    #pullupTrash()
    #way to trash-bin
    collected = moveCollector(field, where,to, collected)
    field[to[0]][to[1]] = 0
    print('to', to)
    return to



def collectTrash(filled, robotpos):
    global collected, finish
    neartrash = findNearestTrash(filled, robotpos)
    if neartrash[1] == 0:
        finish = robotpos
        return robotpos
    else:
        robotpos = removeItem(filled, robotpos, neartrash[0])
        print('cpos', robotpos)
        collectTrash(filled, robotpos)
def setStartPos():        
    fieldNames = ["RobotX","RobotY","BinposX","BinposY"]
    fieldValues = [robot[0],robot[1], tbin[0],tbin[1]]
    fieldValues = e.multenterbox("Wanna to change startpositions?","Ready To Start", fieldNames, fieldValues)
# make sure that none of the fields was left blank
    while 1:
        if fieldValues == None: break
        errmsg = ""
        for i in range(len(fieldNames)):
          if fieldValues[i].strip() == "":
            errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
        if errmsg == "": break # no problems found
        fieldValues = e.multenterbox(errmsg, title, fieldNames, fieldValues)
    robot[0] = int(fieldValues[0])
    robot[1] = int(fieldValues[1])
    print('r',robot[0],robot[1],type(robot[0]))
    place[robot[0]][robot[1]] = 3
    tbin[0] = int(fieldValues[2])
    tbin[1] = int(fieldValues[3])
    place[tbin[0]][tbin[1]] = 2
    print('t',tbin[0],tbin[1], type(tbin[0]))

fname =''
table = []
robot = [0,0]
tbin = [0,0]
placetosave = []
msg = "Start trash program?"
f = None
d = {}
while  e.ccbox(msg, "Collecting Trash"):
    place = [[0 for x in range(100)] for y in range(100)]
    xDim = len(place)
    yDim = len(place[0])
    scale = 5
    xShift = -xDim*scale/2
    yShift = -xDim*scale/2
    collected =0#
    finish = [0,0]#
    robot = [0,0]
    tbin = [0,0]
    p.setpos(0,0)
    p.clear()
    if e.boolbox("", "", ["load map from file", "generate random map"]):
        print("from file")
        fname = e.fileopenbox()
        if fname is None:
            msg = "Mo file name entered. Try from start?"
            continue
        #f = open(fname)
        place = json.load(open(fname))
        ret = findAllTrash(place,trashcoords)
        trashcoors, robot, tbin = ret
        filled = place
    else:
        num = e.integerbox("enter number of trash items +2","","5",3,101)
        ret = fillWithTrash(place, num)
        filled, robot, tbin = ret
    placetosave = copy.deepcopy(place)
    #asking for alternative start positions

    

#drawing this place
    drawAreaTurtle(p,filled)
    #findAllTrash(filled,trashcoords)
    setStartPos()

    placetosave = copy.deepcopy(place)
    collectTrash(place, robot)
    pygame.init()
    pygame.mixer.music.load("end.mp3")
    pygame.mixer.music.play()
    moveCollector(place, finish, tbin, collected)
    time.sleep(15)
if len(placetosave) >=4:
    fname = e.filesavebox("Save last field?","trash collector",fname,'*.txt')
    if  not fname is None:
        json.dump(placetosave, open(fname,'w'))




