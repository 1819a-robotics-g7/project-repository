// Adapted from "dronebotworkshop.com/robot-car-with-speed-sensor"

#include "TimerOne.h"

const byte leftMotor = 2;
const byte rightMotor = 3;


volatile int counterLeft = 0;
volatile int counterRight =0;

const float wheelDiameter = 67.4;
const float robotTrackWidth = 220.00;

float diskslots = 20.00;

void  interruptServiceRoutineLeftMotor()
{
  counterLeft++;
}

void  interruptServiceRoutineRightMotor()
{
  counterRight++;
}

// left motor
int enableA = 10;
int inputA = 9;
int inputB = 8;

// right motor
int enableB = 5;
int inputC = 6;
int inputD = 7;

int convertCentimetersToSteps(float cm)
{
  int result;
  float circumference = (wheelDiameter * 3.14)/10; //in cm
  float centimetersPerStep = circumference / diskslots;
  result = (int)(cm / centimetersPerStep);
  return result;
}

int convertDegreesToSteps(int degreesToTurn)
{
  int result;
  float turningFullCircleInCm = (robotTrackWidth*3.14)/10;
  float fractionOfFullCircleToTurn = degreesToTurn / 360.00;
  float centimetersToTurn = fractionOfFullCircleToTurn * turningFullCircleInCm;

  result = convertCentimetersToSteps(centimetersToTurn);
  return result;
}

void moveForward(int steps, int motorspeed)
{
  counterRight = 0;
  counterLeft = 0;

  // direction forward
  digitalWrite(inputA, HIGH);
  digitalWrite(inputB, LOW);

  digitalWrite(inputC, HIGH);
  digitalWrite(inputD, LOW);

  while (steps > counterRight && steps > counterLeft)
  {
    if (steps > counterRight)
    {
      analogWrite(enableA, motorspeed);
    }
    else
    {
      analogWrite(enableA, 0);
    }

    if (steps > counterLeft)
    {
      analogWrite(enableB, motorspeed);
    }
    else
    {
      analogWrite(enableB, 0);
    }
  }

  //Stop and reset
  analogWrite(enableA, 0);
  analogWrite(enableB, 0);
  counterLeft = 0;
  counterRight = 0;  
}

void moveReverse(int steps, int motorspeed)
{
  counterRight = 0;
  counterLeft = 0;

  // direction reverse
  digitalWrite(inputA, LOW);
  digitalWrite(inputB, HIGH);

  digitalWrite(inputC, LOW);
  digitalWrite(inputD, HIGH);

  while (steps > counterRight && steps > counterLeft)
  {
    if (steps > counterRight)
    {
      analogWrite(enableA, motorspeed);
    }
    else
    {
      analogWrite(enableA, 0);
    }

    if (steps > counterLeft)
    {
      analogWrite(enableB, motorspeed);
    }
    else
    {
      analogWrite(enableB, 0);
    }
  }

  //Stop and reset
  analogWrite(enableA, 0);
  analogWrite(enableB, 0);
  counterLeft = 0;
  counterRight = 0;  
}

void spinRight(int steps, int motorspeed)
{
  counterRight = 0;
  counterLeft = 0;

  // direction right motor reverse, left motor forward
  digitalWrite(inputA, LOW);
  digitalWrite(inputB, HIGH);

  digitalWrite(inputC, HIGH);
  digitalWrite(inputD, LOW);

  while (steps > counterRight && steps > counterLeft)
  {
    if (steps > counterRight)
    {
      analogWrite(enableA, motorspeed);
    }
    else
    {
      analogWrite(enableA, 0);
    }

    if (steps > counterLeft)
    {
      analogWrite(enableB, motorspeed);
    }
    else
    {
      analogWrite(enableB, 0);
    }
  }

  //Stop and reset
  analogWrite(enableA, 0);
  analogWrite(enableB, 0);
  counterLeft = 0;
  counterRight = 0;  
}

void spinLeft(int steps, int motorspeed)
{
  counterRight = 0;
  counterLeft = 0;

  // direction right motor forward, left motor reverse
  digitalWrite(inputA, HIGH);
  digitalWrite(inputB, LOW);

  digitalWrite(inputC, LOW);
  digitalWrite(inputD, HIGH);

  while (steps > counterRight && steps > counterLeft)
  {
    if (steps > counterRight)
    {
      analogWrite(enableA, motorspeed);
    }
    else
    {
      analogWrite(enableA, 0);
    }

    if (steps > counterLeft)
    {
      analogWrite(enableB, motorspeed);
    }
    else
    {
      analogWrite(enableB, 0);
    }
  }

  //Stop and reset
  analogWrite(enableA, 0);
  analogWrite(enableB, 0);
  counterLeft = 0;
  counterRight = 0;  
}


void setup() {
  // Serial monitoring of wheel speeds commented out
  // Serial.begin(9600);
  // Timer1.initialize(1000000); // 1 sec
  attachInterrupt(digitalPinToInterrupt(leftMotor), interruptServiceRoutineLeftMotor, RISING); // counter++ when pin goes high
  attachInterrupt(digitalPinToInterrupt(rightMotor), interruptServiceRoutineRightMotor, RISING);
  // Timer1.attachInterrupt(interruptServiceRoutineTimerOne); // enable timer

 
 
}

void loop() {
  // put your main code here, to run repeatedly:
 delay(1000);
 moveForward(convertCentimetersToSteps(200), 200);
//  moveForward(convertCentimetersToSteps(200), 100);
//  delay(1000);
//  moveReverse(convertCentimetersToSteps(100), 200);
//  delay(1000);
//  spinRight(convertDegreesToSteps(180), 50);
//  delay(1000);
//  moveForward(convertCentimetersToSteps(100), 100);
//  delay(1000);
//  spinLeft(convertDegreesToSteps(360), 25);
}
